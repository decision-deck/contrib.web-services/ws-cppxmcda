/********************************************************************
    Note: two parameters should be provided as input for the command line
	      the first one is the path of the input
		  the second one is the path of the output
	created:201003	
	author:	ZHENG JUN	
	purpose: Resolving INCONSISTENCY	
*********************************************************************/


# include "Parser.h"
# include <iconv.h>
# include <libxml/parser.h>
# include <libxml/xpath.h>
//# include <libxml/xpathInternals.h>
# include <iostream>
# include <vector>
# include<algorithm>
# include <ilcplex/ilocplex.h>
# include <sstream>

using namespace std;


//#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.VC80.CRT' version='8.0.50608.0' processorArchitecture='X86' publicKeyToken='1fc8b3b9a1e18e3b' language='*'\"")

void DisplayChild(xmlNodePtr pChild)
{

xmlChar *str  = xmlNodeGetContent(pChild) ; //获取结点的值

cout <<str<<endl ;

}
class constraint
{

public:	
   string name;
  
   vector<double> consmatrix;

   string id;

   constraint(string nameofcon, vector< double > con,string alternativename)
   {
    name=nameofcon;

    consmatrix=con;

	id=alternativename;


   }
    constraint()
   {
    

   }
  void getvalue(string nameofcon, vector< double > con)
   {
	   name=nameofcon;

       consmatrix=con;

   }
  
};

int main (int argc, char* argv[])

{
xmlKeepBlanksDefault (0);

string inputName, outputName,szDocName;
xmlNodeSetPtr NodeSetPtr;
 //get the path of document to be readed from command line

//cout<<"argc"<<argc<<endl;

    if (argc <= 2) 

    {
      cout<<"The input parameters are incomplete!"<<endl;

       return(0);

    }

	inputName = argv[1];	

	szDocName =  inputName + "/methodParameters.xml";
	xmlDocPtr doc_methodParameters = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_methodParameters!=NULL) cout<<"The methodParameters file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/criteriaLinearConstraints.xml";
	xmlDocPtr doc_criteriaLinearConstraints = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_criteriaLinearConstraints!=NULL) cout<<"The criteriaLinearConstraints file has been successfully opened!"<<endl;
	else  return NULL;
	 

//read parameter provided by DM
    int maxcount;
    string resolutioncriterion;
	int confidencelevel;

	maxcount=getXmlInt(doc_methodParameters,"/xmcda:XMCDA/methodParameters/parameters/parameter[@id='maxcount']/value");

    resolutioncriterion=getXmlString(doc_methodParameters,"/xmcda:XMCDA/methodParameters/parameters/parameter[@id='resolutioncriterion']/value");
	
	if(resolutioncriterion=="confidencelevel")
	confidencelevel=getXmlInt(doc_methodParameters,"/xmcda:XMCDA/methodParameters/parameters/parameter[@id='confidencelevel']/value");
	else if(resolutioncriterion=="cardinality")confidencelevel=10;
	else {cout<<"Wrong inconsistency resolution criterion(either confidencelevel or cardinality"; return NULL;}
	
cout<<"The method parameters have been successfully read!"<<endl;


   
IloEnv   env;
 try{

     IloModel model(env); //define a model    
     IloNumVarArray x(env); //define varibles
     IloRangeArray con(env);  //define constaints
     IloExpr expr(env);
     IloExpr exprsum(env);
     IloNumArray vals(env);	 
     vector< string > criname;//define the vector for names of criteria
     
  
   	 xmlNodePtr NodePtr;
 
 cout<<"Begin to read the constraints!"<<endl;
//read inconsistency matrix and tranfer it to constraints in cplex and judge whether the constaints contain enough information
	 IloNumVarArray bin(env); 
     string  str,strXPath;
     NodeSetPtr=get_nodeset(doc_criteriaLinearConstraints,"/xmcda:XMCDA/criteriaLinearConstraints/constraint");
	 int cons_size=NodeSetPtr->nodeNr;
	 vector< int > cfd;
	 ostringstream oss;
 cout<<"There are "<< cons_size<<" constraints!"<<endl;
	 vector< string > conname(NodeSetPtr->nodeNr);//define the vector for names of constraints
     vector< string >::iterator iiter_conname= conname.begin();
	 int sign_name=0;
	 int sign_element=0;
	 int sign_rhs=0;
	 int sign_operator=0;
     
	 for(int i=0;i<cons_size;i++)
   {
   	   int confidence=0;   //for each constraint, its confidence should be initialised to 0, as when resolutioncriterion is cardinality, the confidence level will not be read.
       NodePtr=NodeSetPtr->nodeTab[i]->xmlChildrenNode;

cout<<"Reading"<< i<<" th constraint..."<<endl;   

       //read element
	    while(NodePtr!= NULL)
		{   
			//DisplayChild(NodePtr);
            if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"name")))  
	        { 

		     str =(const char*)xmlNodeGetContent(NodePtr);
		    (*(iiter_conname+i))=str; 
		     NodePtr = NodePtr->next;
cout<<"Reading name of the constraint..."<<endl;  
			}
            else
		    {  cout<<"Warning! The "<<i+1<<" th constraint don't have name!"<<endl;
			   oss<<"Constraint "<<i+1;
			  (*(iiter_conname+i))=oss.str();

			}

    	
			if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"element")))  
		      {   

				  int nIndexcri;
	              xmlNodePtr NodePtrChild;			          
cout<<"Reading element of the constraint..."<<endl; 
                   while((!xmlStrcmp(NodePtr->name, (const xmlChar *)"element")))
	               {

					   NodePtrChild=NodePtr->xmlChildrenNode;
				     if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"criterionID")))
		                {

			               str = (const char*)xmlNodeGetContent(NodePtrChild); 
cout<<"Reading criterionID of the element..."<<endl; 					    				         			 
			            }//reading criterionID
				      if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"variable")))
		            {
			            str =( const char*)xmlGetProp(NodePtrChild,BAD_CAST "id");
cout<<"Reading lamda of the element..."	<<endl; 					
			        }	//reading lamda

			           nIndexcri = find(criname.begin(), criname.end(), str) - criname.begin();
			       	   NodePtrChild=NodePtrChild->next;
				
				       double temp =atof((const char*)xmlNodeGetContent(NodePtrChild));//coefficient
cout<<"Reading coefficient of the element..."<<endl; 		
				       if(nIndexcri>=criname.size())
				       {
					    criname.push_back(str);				       
				        x.add(IloNumVar(env)); 
				        expr+= temp*x[(criname.size()-1)];
				
				       }
					   else   expr+= temp*x[nIndexcri];
                      NodePtr = NodePtr->next;
					
				   }

		}//end of reading element
        else
		    {  cout<<"The constraint "<<(*(iiter_conname+i))<<" don't have element!"<<endl; return NULL;   }

	   if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"rhs")))  
	   { 

	      double temp =atof((const char*)xmlNodeGetContent(NodePtr)); 
cout<<"Reading rhs of the constraint..."<<endl; 
		// cout<<temp<<endl;
		  expr+= (-1)*temp; 
	   }//end of reading rhs
       else
		    {cout<<"The constraint "<<(*(iiter_conname+i))<<" don't have rhs!"<<endl; return NULL;   }

	   NodePtr = NodePtr->next; 
     	  
      if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"operator")))  
	   { 

cout<<"Reading operator of the constraint..."<<endl; 
         //cout<<NodePtr->name<<endl;
		 str =(const char*)xmlNodeGetContent(NodePtr);
		 NodePtr = NodePtr->next;
		// cout<<NodePtr->name<<endl;	

		 if(resolutioncriterion=="confidencelevel")
		 {
cout<<"Reading confidencelevel of the constraint if the resolution is obtained according to the confidencelevel..."<<endl; 			
			if(NodePtr==NULL){cout<<"The constraint "<<(*(iiter_conname+i))<<" don't have confidence information"<<endl; return NULL;}

            xmlNodePtr childnode=NodePtr->xmlChildrenNode->xmlChildrenNode;
		    while(childnode!=NULL)
		    {if ((!xmlStrcmp(childnode->name, (const xmlChar *)"rank")))  
		 	  {confidence = atof((const char*)xmlNodeGetContent(childnode)); 
		       cfd.push_back(confidence);
			   break;
			   }
		     else childnode=childnode->next;
			}
			  NodePtr = NodePtr->next; 
		 }
		 else 
		 { 
			 
			 if(NodePtr!=NULL)  NodePtr = NodePtr->next; 
			 cfd.push_back(0);
		 }

		 if((cfd.size()!=(i+1))& (resolutioncriterion=="confidencelevel"))
		 {cout<<"The constraint "<<(*(iiter_conname+i))<<" don't have 'rank' tag in its confidence information"<<endl; return NULL;}//judge whether the confidence information is complete

		 if(confidence<=confidencelevel)
          {		   
			bin.add(IloBoolVar(env));
		    if(str=="geq")	  {  expr+= 10*bin[(bin.getSize()-1)]; con.add(expr>= 0); }
            if(str=="leq")	  {  expr+= -10*bin[(bin.getSize()-1)]; con.add(expr<= 0); }
            if(str=="eq")	  {  expr+= 10*bin[(bin.getSize()-1)]; con.add(expr== 0); }
			exprsum+= bin[(bin.getSize()-1)];	
cout<<"Add constraints which can be deleted to cplex model "<<endl; 
		  }
		 else
	      { 
		   if(str=="geq")	{   con.add(expr>= 0); }
           if(str=="leq")	{  con.add(expr<= 0); }
           if(str=="eq")	{  con.add(expr== 0); }
cout<<"Add constraints which cannot be deleted to cplex model"<<endl; 
          }
	   }//end of reading operator

	   else
		    {  cout<<"The constraint "<<(*(iiter_conname+i))<<" don't have operator!"<<endl; return NULL;   }

	   // NodePtr = NodePtr->next; 
       
		
	}//end for one constraint
   
    expr.clear(); confidence=0;  oss.str("");
}
	 
	 
cout<<"Defining cplex model..."<<endl;	 
     model.add(con);
     model.add(IloMinimize(env, exprsum));
	 IloCplex cplex(model); 
	 //cplex.exportModel("lpex1.lp");
     int numdel;
	 int indexdel;
	 int counter=0;
	 string filename;	

cout<<"Creating Suggestion file..."<<endl;
      xmlNodePtr methodMessages_node;
	  xmlDocPtr docnew_methodMessages= createXmlFile();
	  xmlNodePtr root_node_methodMessages=xmlDocGetRootElement(docnew_methodMessages);
	  methodMessages_node = xmlNewNode(NULL,BAD_CAST"methodMessages");
      xmlAddChild(root_node_methodMessages,methodMessages_node);

cout<<"Creating criteriaLinearConstraints file..."<<endl;
	   xmlDocPtr docnew_criteriaLinearConstraints=createXmlFile();
       xmlNodePtr root_node_criteriaLinearConstraints=xmlDocGetRootElement(docnew_criteriaLinearConstraints); 

      while ( cplex.solve())
	 { 
cout<<"The model is solved..."<<endl;
       cplex.getValues(x,vals);
	   //env.out() << "weights        = " << vals << endl;
	   //env.out() << "Solution value  = " << cplex.getObjValue() << endl;
	   cplex.getValues(bin,vals);
	   //env.out() << "Values        = " << vals << endl;
       //env.out() << "Solution status = " << cplex.getStatus() << endl;
	  
      // env.out() << "Solution value  = " << cplex.getObjValue() << endl;
       oss.str("");
	   numdel=0; 
	   xmlNodePtr node,childnode,grandchild;
	  // cout<<"The algorithm based on binary linear program.The suggestions for deleted constraints are:"<<endl;
	   
	   oss<<"Suggestion "<<(counter+1)<<" "<<"is to delete the constraints:";
	    
cout<<"Writing new message..."<<endl;        
       
       childnode = xmlNewNode(NULL,BAD_CAST"message");
       grandchild = xmlNewNode(NULL,BAD_CAST"text");
       xmlAddChild(childnode,grandchild);
       xmlAddChild(methodMessages_node,childnode);

 cout<<"Writing criterialinearconstraints..."<<endl;        
	    node = xmlNewNode(NULL,BAD_CAST"criteriaLinearConstraints");
        xmlAddChild(root_node_criteriaLinearConstraints,node);       	   

       int countercfd=0;
	   for(indexdel=0;indexdel<cons_size;indexdel++)  
		{	      
		
		 if(cfd[indexdel]<=confidencelevel)//the reason for checking confidence level of each constraint is that the number of binary variables is the same with the number of constraints which can be deleted
		 { 			 
			 if (vals[countercfd]==1)
		       { expr+=bin[countercfd];
		         numdel++;
			     oss<<" "<<(*(iiter_conname+indexdel));
           cout<<"copying the deleted constraints..."<<endl; 			
			   //copy the deleted constraints to suggestion.xml
		  
               NodeSetPtr = get_nodeset(doc_criteriaLinearConstraints,"/xmcda:XMCDA/criteriaLinearConstraints/constraint");
               childnode=xmlCopyNode(NodeSetPtr->nodeTab[indexdel],1);
               xmlAddChild(node,childnode);
			  // DisplayChild(childnode);
			  // childnode = xmlNewNode(NULL,BAD_CAST"constraint");
			 //  xmlAddChild(node,childnode);			  
			 //  cout<<indexdel<<endl;
			 }
               countercfd++;	   
		 }//if the constraint can be deleted, judge whether it is nessecary to do so
    
	   }//end of getting message names
	 
	   str=oss.str();       
	   childnode= xmlNewCDataBlock(docnew_methodMessages,BAD_CAST(str.c_str()),str.length());
       xmlAddChild(grandchild,childnode);

cout<<"End of writing this message..."<<endl;    

       if((numdel>=1)&&(numdel<maxcount))
		{   
	      cout<<"Finding new suggestions..."<<endl; 	 	 
		  con.add(expr<=(numdel-1));		
		  model.add(con);
	      IloCplex cplex(model); 
	      counter++;		
		}
	    else break;
	 	 
	  }

     int writefile;
     outputName = argv[2];
	 oss.str("");
	 oss <<outputName <<"/suggestions.xml";    
	 str=oss.str();
	 oss.str("");

	 writefile=xmlSaveFileEnc(str.c_str(),docnew_methodMessages,"UTF-8");
	 if(writefile==-1)
	 {   cout<<"Failed to write result to xml file!"<<endl;
		 return NULL;}
	 else cout<<"The output file for suggestions is saved..."<<endl; 
     closeXmlFile(docnew_methodMessages);

	 oss <<outputName <<"/constraints_suggestions.xml";    
	 str=oss.str();
	 oss.str("");
	 writefile=xmlSaveFileEnc(str.c_str(),docnew_criteriaLinearConstraints,"UTF-8");
	 if(writefile==-1)
	 {   cout<<"Failed to write the deleted constraints for suggestion to xml file!"<<endl;
		 return NULL;}
	 else cout<<"The output file for the deleted constraints is saved..."<<endl;
     closeXmlFile(docnew_criteriaLinearConstraints);

	}//end of the algorithm to resolve inconsistency

    
   catch (IloException& e)
   {
      cerr << "Concert exception caught: " << e << endl;
   }
   catch (...) {
      cerr << "Unknown exception caught" << endl;
   }
   
   closeXmlFile(doc_methodParameters);
   closeXmlFile(doc_criteriaLinearConstraints);
   env.end();
   cout<<"The program is ended successfully!"<<endl;
   return 0;


}