/********************************************************************
    Note: two parameters should be provided as input for the command line
	      the first one is the path of the input
		  the second one is the path of the output
	created:201003	
	author:	ZHENG JUN	
	purpose: TEST INCONSISTENCY	
*********************************************************************/
# include "Parser.h"
# include <iconv.h>
# include <libxml/parser.h>
# include <libxml/xpath.h>
# include <iostream>
# include <vector>
# include <algorithm>
# include <ilcplex/ilocplex.h>
# include <sstream>



using namespace std;


//#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.VC80.CRT' version='8.0.50608.0' processorArchitecture='X86' publicKeyToken='1fc8b3b9a1e18e3b' language='*'\"")

int numcri; 
int numcat;
int numalt;
bool consistencyresult;

 class constraint
{

public:	
   string name;
  
   vector<double> consmatrix;

   string id;

  constraint(string nameofcon, vector< double > con,string alternativename)
   {
    name=nameofcon;

    consmatrix=con;

	id=alternativename;


   }
    constraint()
   {
    

   }
  void getvalue(string nameofcon, vector< double > con)
   {
	   name=nameofcon;

       consmatrix=con;

   }
  
};


void writevector(vector< constraint > &cons,vector< double > &weights,ostringstream &oss,string alternativename)
{

//define the vector for the weights of one constraint

vector< double > weightszero(numcri+2, 0);
string dcr;
dcr=oss.str();
constraint con1(dcr,weights,alternativename);
cons.push_back(con1);
weights=weightszero;
oss.str("");


}
void ComputeCI(vector< int >::iterator direction,vector< double >::iterator c,int e,vector< double >::iterator eval,vector< double >::iterator g,vector< double >::iterator p, vector< double >::iterator q);


void Addcon(vector< int >::iterator direction,int lowcat, int upcat, int exampindex, IloModel model, IloNumVarArray x, IloRangeArray con,vector< double >::iterator c,vector< double >::iterator eval,
			vector< double >::iterator g,vector< double >::iterator q,vector< double >::iterator p,vector< constraint > &cons, string alternativename,vector< double >::iterator d)
{
 // write to constraint vectors
vector< double > weights(numcri+2);
vector< double > weights1(numcri+2);
ostringstream oss;

//////////////////
	
IloEnv env = model.getEnv();

// compute partial concordance index of the assigned alternative
        
 ComputeCI(direction,c,exampindex,eval,g,p,q);

 //add constaints

 IloExpr expr1(env);

 IloExpr expr2(env);

int index;

if ((lowcat==1)||(upcat==numcat))
{
if ((lowcat==1)&&(upcat!=numcat))
{for (index = 0; index < numcri; index++)
{
expr2 += (*(c+(upcat-1)*numcri+index)) * x[index];  
 weights[index]= -1*(*(c+(upcat-1)*numcri+index));
 }

  expr2= expr2*(*(d+(exampindex*(numcat-1)+upcat-1)));// add veto
  expr2 +=- x[numcri];  
  weights[numcri]=1;
  weights[numcri+1]=0.0001;//sigma(a, bh)<lamda strictly
  oss<<"C["<<alternativename<<"]<="<<upcat;// change the form of name to make it clear
  writevector(cons,weights,oss,alternativename);

  x.add(IloNumVar(env));
  con.add(-1*expr2 - x[(x.getSize()-1)]== 0);
  con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);
}

if((lowcat!=1)&&(upcat==numcat))
  {for (index = 0; index < numcri; index++)
{
	expr1 += (*(c+(lowcat-2)*numcri+index)) * x[index];
    weights[index]= (*(c+(lowcat-2)*numcri+index));
}
   expr1= expr1*(*(d+(exampindex*(numcat-1)+lowcat-2)));  // add veto

   expr1 += - x[numcri];
   weights[numcri]=-1;
   oss<<"C["<<alternativename<<"]>="<<lowcat;
   writevector(cons,weights,oss,alternativename);

   x.add(IloNumVar(env));
   con.add(expr1- x[(x.getSize()-1)]== 0);
   con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);
  }
}
else
{ 
   for (index = 0; index < numcri; index++)
   { 
	 expr1 += (*(c+(lowcat-2)*numcri+index)) * x[index];
	  weights[index]= (*(c+(lowcat-2)*numcri+index));
	 expr2 += (*(c+(upcat-1)*numcri+index)) * x[index]; 
	  weights1[index]= -1*(*(c+(upcat-1)*numcri+index));
      }

    expr1= expr1*(*(d+(exampindex*(numcat-1)+lowcat-2)));  // add veto
    expr2= expr2*(*(d+(exampindex*(numcat-1)+upcat-1)));// add veto

    x.add(IloNumVar(env));
    expr1 +=- x[numcri];
    weights[numcri]=-1;
	oss<<"C["<<alternativename<<"]>="<<lowcat;
	writevector(cons,weights,oss,alternativename);

    con.add(expr1- x[(x.getSize()-1)]== 0);
    con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);

   x.add(IloNumVar(env));
   expr2 +=- x[numcri]; 
   weights1[numcri]=1;
   weights1[numcri+1]=0.0001;//sigma(a, bh)<lamda strictly
   oss<<"C["<<alternativename<<"]<="<<upcat;
   writevector(cons,weights1,oss,alternativename);

   con.add(-1*expr2 - x[(x.getSize()-1)]== 0);
   con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);




}// for else



}// for end


 void ComputeCI(vector< int >::iterator direction,vector< double >::iterator c,int e,vector< double >::iterator eval,vector< double >::iterator g,vector< double >::iterator p, vector< double >::iterator q)
{
    
	int i;
	int j;
//judge the direction of the criteria	
for(i=0;i<(numcat-1);i++)
	for(j=0;j<numcri;j++)
	{    
	    if(*(direction+j)==-1)
		{if(*(p+j)==0)
	   {if(*(g+i*numcri+j)<*(eval+e*numcri+j))   *(c+i*numcri+j)=0;
	    	else *(c+i*numcri+j)=1;		   }
	     else
		 *(c+i*numcri+j)=(*(p+j)-min(*(eval+e*numcri+j)-*(g+i*numcri+j), *(p+j)))/(*(p+j)-min(*(eval+e*numcri+j)-*(g+i*numcri+j), *(q+j)));}
	

		 else
		{ 
		if(*(p+j)==0)
		{if(*(g+i*numcri+j)>*(eval+e*numcri+j))  *(c+i*numcri+j)=0;
	    	else *(c+i*numcri+j)=1;
		}
	     else
		 *(c+i*numcri+j)=(*(p+j)-min(*(g+i*numcri+j)-*(eval+e*numcri+j), *(p+j)))/(*(p+j)-min(*(g+i*numcri+j)-*(eval+e*numcri+j), *(q+j)));
		
		
		}
}

}


int main(int argc, char* argv[])
{	
	
xmlKeepBlanksDefault (0);
 
// evaluation matrix
 vector <double> eval;

//limits of categories matrix
 vector <double> g;
 
 //indiffernce thresholds matrix
 vector <double> q;

 // preference thresholds matrix
 vector <double> p;

  //veto matrix
vector <double> v;
 
 //thresholds matrix
 vector <double> t;

//cut limit
vector <double> cut;

//define lower and upper category matrix
vector <int> range;

//define direction of preference matrix
vector <int> direction;

//define the constraints of weights provided by DM
vector <double> conswgt;

//define the vector for names of criteria
vector< string > criname;

//define the vector for names of alternatives
vector< string > altname;
//define the vector for names of criteria
vector< string > catname;

string inputName, outputName,szDocName;
xmlNodeSetPtr NodeSetPtr;
 //get the path of document to be readed from command line

//cout<<"argc"<<argc<<endl;

    if (argc <= 2) 

    {
      cout<<"The input parameters are incomplete!"<<endl;

       return(0);

    }

	inputName = argv[1];	

	szDocName =  inputName + "/alternatives.xml";
	xmlDocPtr doc_alternatives = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_alternatives!=NULL) cout<<"The alternatives file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/categories.xml";
	xmlDocPtr doc_categories = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categories!=NULL) cout<<"The categories file has been successfully opened!"<<endl;
	else   return NULL;

	szDocName =  inputName + "/categoriesComparisons.xml";
	xmlDocPtr doc_categoriesComparisons = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categoriesComparisons!=NULL) cout<<"The categoriesComparisons file has been successfully opened!"<<endl;
    else   return NULL;
    
	szDocName =  inputName + "/criteriaLinearConstraints.xml";
	xmlDocPtr doc_criteriaLinearConstraints = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_criteriaLinearConstraints!=NULL) cout<<"The criteriaLinearConstraints file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/categoriesProfiles.xml";
	xmlDocPtr doc_categoriesProfiles = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categoriesProfiles!=NULL) cout<<"The categoriesProfiles file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/criteria.xml";
	xmlDocPtr doc_criteria = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_criteria!=NULL) cout<<"The criteria file has been successfully opened!"<<endl;
    else  return NULL;

	szDocName =  inputName + "/performanceTable.xml";
	xmlDocPtr doc_performanceTable = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_performanceTable!=NULL)	cout<<"The performanceTable file has been successfully opened!"<<endl;
	else   return NULL;

	szDocName =  inputName + "/alternativesAffectations.xml";
	xmlDocPtr doc_alternativesAffectations = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_alternativesAffectations!=NULL)	cout<<"The alternativesAffectations file has been successfully opened!"<<endl;
	else cout<<"No assignment example!"<<endl;             

cout<<"The input file for the sorting problem has been successfully opened!"<<endl;
//read criteria related data

cout<<"Begin to read criteria related data..."<<endl;

	NodeSetPtr = get_nodeset(doc_criteria,"/xmcda:XMCDA/criteria/criterion");
	if (NodeSetPtr == NULL) 
	{
		return NULL;  //if the set of criteria is null, the program is ended
	}

	numcri= NodeSetPtr->nodeNr;
	
   for (int i=0; i<numcri; ++i)
  {
	//get criterion name
  cout<<"Begin to read "<<i<<" th criteria..."<<endl;
     string strXPath,temp;
	 double tempdata;

	 xmlChar* szAttr = xmlGetProp(NodeSetPtr->nodeTab[i],BAD_CAST "id");

	 if(szAttr==NULL)	 return NULL;

	 criname.push_back(( char *)szAttr);
 cout<<"Begin to read criterion preference direction information..."<<endl;

	 //criterion preference direction information 
	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
     strXPath += "\']/scale";
	 temp=getXmlString(doc_criteria,strXPath.c_str());
	 
	 if(temp=="NULL")	 return NULL;     

     if(temp=="max")   direction.push_back(1);
	 else   direction.push_back(-1);

cout<<"End of reading criterion preference direction information..."<<endl;

cout<<"Begin to read thresholds information..."<<endl;

//read thresholds
	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/thresholds/threshold[@mcdaConcept='pref']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());

     if(tempdata==1e6)	 return NULL; 
	 else p.push_back(tempdata);

	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
   	 strXPath += "\']/thresholds/threshold[@mcdaConcept='ind']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());
	  if(tempdata==1e6)	 return NULL; 
	 else q.push_back(tempdata);
	 
	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/thresholds/threshold[@mcdaConcept='veto']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());
	 if(tempdata==1e6)	 return NULL; 
	 else if(tempdata==0) tempdata=1000;
	 v.push_back(tempdata);
     cout<<"End of reading thresholds information..."<<endl;

   }
 
  //end of read criteria related data


//read alternative evaluation matrix
cout<<"Begin to getting alternative evaluation matrix..."<<endl;

    NodeSetPtr = get_nodeset(doc_alternatives,"/xmcda:XMCDA/alternatives/alternative[active='true']");
	if (NodeSetPtr == NULL) 
	{
		return NULL;  //if the set of criteria is null, the program is ended
	}

	numalt= NodeSetPtr->nodeNr; 
	
   for (int i=0; i<numalt; ++i)
   { 
   
  cout<<"Begin to read "<<i<<" th alternative..."<<endl;   
	   //get alternative name

  cout<<"Begin to get alternative name..."<<endl;

     string strXPath,temp;
	 double tempdata;

	 xmlChar* szAttr = xmlGetProp(NodeSetPtr->nodeTab[i],BAD_CAST "id");
	 altname.push_back(( char *)szAttr);
 cout<<"End of getting alternative name..."<<endl;

     //get performance of every alternative 
  cout<<"Begin to get performance for alternative..."<<endl;
	 for (int j=0; j<numcri; ++j)
	 {
	 
	 strXPath="/xmcda:XMCDA/performanceTable/alternativePerformances[alternativeID=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/performance[criterionID=\'";
	 strXPath += criname[j];
	 strXPath += "\']/value";
	 tempdata=getXmlDouble(doc_performanceTable,strXPath.c_str());
	  if(tempdata==1e6)	 return NULL;     
	 eval.push_back(tempdata);   
	 }
   
 cout<<"End of getting performance for alternative..."<<endl;
   }
 cout<<"End of getting alternative evaluation matrix..."<<endl;  
// end of read alternative evaluation matrix

//read profiles data
cout<<"Begin to read profiles data..."<<endl;

  string strXPath,str;
  xmlNodePtr NodePtr;  

  NodeSetPtr = get_nodeset(doc_categories,"/xmcda:XMCDA/categories/category");
  if (NodeSetPtr == NULL)		return NULL;  //if the set of criteria is null, the program is ended
	
  numcat= NodeSetPtr->nodeNr; 
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial";
  NodeSetPtr = get_nodeset(doc_categoriesComparisons,strXPath.c_str());
  if (NodeSetPtr == NULL)		return NULL;
  str=getXmlString(doc_categoriesComparisons,strXPath.c_str());


while(NodeSetPtr!=NULL)
{ 
  NodePtr=NodeSetPtr->nodeTab[0]->parent;
  NodePtr=NodePtr->xmlChildrenNode;

  while(NodePtr != NULL)
    { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"initial")))       
           str = (const char*)xmlNodeGetContent(NodePtr);     
      NodePtr = NodePtr->next;   
   }
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/terminal[categoryID=\'";
  strXPath += str;
  strXPath += "\']";
  
  //use the code in get_nodeset function, because we don't want to output the error message here
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result;
  context = xmlXPathNewContext(doc_categoriesComparisons);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
   if (result == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result->nodesetval)) 
	{
		xmlXPathFreeObject(result);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result->nodesetval;
}
catname.push_back(str);//find the lowest category

cout<<"The lowest category is found..."<<endl;

//write category name in order
cout<<"Begin to write category name in order..."<<endl;

strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial[categoryID=\'";
strXPath += str;
strXPath += "\']";
NodeSetPtr = get_nodeset(doc_categoriesComparisons,strXPath.c_str()); 
while(NodeSetPtr!=NULL)
{ 
  NodePtr=NodeSetPtr->nodeTab[0]->parent;
  NodePtr=NodePtr->xmlChildrenNode;

  while(NodePtr != NULL)
    { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"terminal")))       
         { str = (const char*)xmlNodeGetContent(NodePtr); 
           catname.push_back(str);
         }
      NodePtr = NodePtr->next;   
   }
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial[categoryID=\'";
  strXPath += str;
  strXPath += "\']";
  
  //use the code in get_nodeset function, because we don't want to output the error message here
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result;
  context = xmlXPathNewContext(doc_categoriesComparisons);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
   if (result == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result->nodesetval)) 
	{
		xmlXPathFreeObject(result);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result->nodesetval;
}

cout<<"End of writing category name in order..."<<endl;
//write category name in order

cout<<"Begin to read performance of category limits..."<<endl;
//read performance of category limits
for (int i=0; i<(numcat-1); ++i)
{
  cout<<"Begin to read performance of category limit for "<<catname[i]<<"..."<<endl;

  strXPath="/xmcda:XMCDA/categoriesProfiles/categoryProfile/limits/lowerCategory[categoryID=\'";	
  strXPath += catname[i];
  strXPath += "\']";
  NodeSetPtr = get_nodeset(doc_categoriesProfiles,strXPath.c_str());
  NodePtr=NodeSetPtr->nodeTab[0]->parent->parent;
  NodePtr=NodePtr->xmlChildrenNode;
 
  while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"alternativeID")))       
        str = (const char*)xmlNodeGetContent(NodePtr);         
        NodePtr = NodePtr->next;   
   }
   for (int j=0; j<numcri; ++j)
	 {double tempdata;		 
	 strXPath="/xmcda:XMCDA/performanceTable/alternativePerformances[alternativeID=\'";
     strXPath += str;
  	 strXPath += "\']/performance[criterionID=\'";
	 strXPath += criname[j];
	 strXPath += "\']/value";
	 tempdata=getXmlDouble(doc_performanceTable,strXPath.c_str());
	 g.push_back(tempdata);
	 }
 cout<<"End of reading performance of category limit for "<<catname[i]<<"..."<<endl;

}
cout<<"End of reading performance of category limits..."<<endl;
cout<<"End of reading profiles data..."<<endl;
//end of read performance of category limits

//initialize range matrix(1,numcat)
cout<<"Initializing range matrix..."<<endl;
for (int i=0;i<numalt;i++)
 for (int j=0;j<2;j++)
 {
	 if (j==0) range.push_back(1);
 else  range.push_back(numcat);
 }

 //get iterator for the matrix which have been initialized

vector< double >::iterator iiter1 = eval.begin();

 vector< double >::iterator iiter2 = g.begin();

 vector< double >::iterator iiter3 = q.begin();
 
 vector< double >::iterator iiter4 = p.begin();

 vector< double >::iterator iiter11 = t.begin();

vector< int>::iterator iiter12 = range.begin();

vector< int >::iterator iiter13 = direction.begin();

vector< string >::iterator iiter_criname= criname.begin();

vector< string >::iterator iiter_altname= altname.begin();;

vector< string >::iterator iiter_catname= catname.begin();

cout<<"Defining matrix..."<<endl;
 // weights
vector <double> w(1*numcri);
vector< double >::iterator iiter5 = w.begin();

//define partial concordance index matrix
vector <double> c((numcat-1)*numcri);
vector< double >::iterator iiter7 = c.begin();

//define disconcordance index matrix
vector <double> d(numalt*(numcat-1));

//define comprehensive concordance index matrix
vector <double> cc(numalt*(numcat-1));
vector< double >::iterator iiter8 = cc.begin();

//define result matrix
vector <int> result(1*numalt);
vector< int >::iterator iiter9 = result.begin();

//define robust conclusion matrix
vector <int> robust(numalt*numcat);
vector< int >::iterator iiter10 = robust.begin();

//define the vector for the weights of one constraint
vector< double > weights(numcri+2);
vector< double > weightszero(numcri+2, 0);

//compute discordance index
cout<<"Begin to compute discordance index..."<<endl; 
vector< double > dj(numcri,0);
for(int e=0;e<numalt;e++)
for(int i=0;i<(numcat-1);i++)
  {   double discordancej=1;
	for(int j=0;j<numcri;j++)
	{  
	    if(direction[j]==-1)//judge the direction of the criteria
		{
		dj[j]=(v[j]-min(eval[e*numcri+j]-g[i*numcri+j], v[j]))/(v[j]-min(eval[e*numcri+j]-g[i*numcri+j], (p[j]+0.75*(v[j]-p[j]))));
		dj[j]= 1-dj[j];
		}
	

		 else
		{ 
		dj[j]=(v[j]-min(g[i*numcri+j]-eval[e*numcri+j], v[j]))/(v[j]-min(g[i*numcri+j]-eval[e*numcri+j], (p[j]+0.75*(v[j]-p[j]))));
		dj[j]= 1-dj[j];
		}
       	discordancej=discordancej*(1-dj[j]);

	}
	*(d.begin()+e*(numcat-1)+i)=discordancej;
}
cout<<"End of computing discordance index..."<<endl;  

//read assignment examples provided by DM
//use the code in get_nodeset function, because we don't want to output the error message here

cout<<"Begin to read assignment examples provided by DM..."<<endl; 

  strXPath= "/xmcda:XMCDA/alternativesAffectations/alternativeAffectation";
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result0;
  context = xmlXPathNewContext(doc_alternativesAffectations);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result0 = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
  if (result0 == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result0->nodesetval)) 
	{
		xmlXPathFreeObject(result0);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result0->nodesetval;
 

 if (NodeSetPtr != NULL)  //if the set is null, it means that there is no assignment example
	{
		for (int i=0;i<NodeSetPtr->nodeNr;i++)
		{  
	cout<<"Begin to read "<<i<<" th assignment examples provided by DM..."<<endl;    
	   int nIndexalt,nIndexcat;
	   NodePtr=NodeSetPtr->nodeTab[i]->xmlChildrenNode;
       while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"alternativeID")))       
	 { str = (const char*)xmlNodeGetContent(NodePtr); 	 
	   nIndexalt = find(altname.begin(), altname.end(), str) - altname.begin();
	  // cout<<nIndexalt<<endl;
	   if(nIndexalt>=numalt){cout<<"Alternative "<<str<<" is not a valid alternative, please check!"; return NULL;}
	 }
	   if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"categoriesInterval")))       
	   { xmlNodePtr NodePtrChild;
		  NodePtrChild=NodePtr->xmlChildrenNode;
           while(NodePtrChild != NULL)
	          {
	            if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"lowerBound")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcat = find(catname.begin(), catname.end(), str) - catname.begin();
	    		if(nIndexcat>=numcat){cout<<"The lower bound of "<<altname[nIndexalt]<<" is not a valid category bound, please check!";return NULL;}
			    range[nIndexalt*2]=nIndexcat+1;

		      }
	           if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"upperBound")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcat = find(catname.begin(), catname.end(), str) - catname.begin();
	         	if(nIndexcat>=numcat){cout<<"The upper bound of "<<altname[nIndexalt]<<" is not a valid category bound, please check!";return NULL;}
			    range[nIndexalt*2+1]=nIndexcat+1;
		       }
               NodePtrChild = NodePtrChild->next;
		      }
	 }
     NodePtr = NodePtr->next;  
		
	   }
   cout<<"End of reading "<<i<<" th assignment examples provided by DM..."<<endl;   
	 }
 }
//end of read alternative range
cout<<"End of reading read assignment examples provided by DM..."<<endl; 

//define the string for the description of one constraint
string dcr;
//define the vector for many constraints
vector <constraint> cons;


IloEnv   env;
try{
    IloModel model(env); //define a model    
    IloNumVarArray x(env); //define varibles
    IloRangeArray con(env);  //define constaints

//add weights varibles 
cout<<"Add weights varibles..."<<endl;   
string filename;
ostringstream oss;
	for(int count=0;count<numcri;count++)
{ 
	x.add(IloNumVar(env));    
	oss<<"w"<<count;
	filename=oss.str();
	x[(x.getSize()-1)].setName(filename.c_str());
	oss.str(" ");
	
}
//add lamda varible 
cout<<"Add lamda and objective function..."<<endl; 
   x.add(IloNumVar(env));
   x[numcri].setName("lamda");
//add obj varible
   x.add(IloNumVar(env));        
   x[numcri+1].setName("minerr");

//add constraints of weights and lamda provided by DM
cout<<"Begin to add constraints of weights and lamda provided by DM..."<<endl; 

 IloExpr exprconswgt(env);
 NodeSetPtr = get_nodeset(doc_criteriaLinearConstraints,"/xmcda:XMCDA/criteriaLinearConstraints/constraint");
 if (NodeSetPtr == NULL) 		return NULL;  //if the set of criteria is null, the program is ended
	

	 for (int i=0;i<NodeSetPtr->nodeNr;i++)
	 {
	  cout<<"Begin to add "<<i<<" th constraint of weights and lamda provided by DM..."<<endl; 
 
	   NodePtr=NodeSetPtr->nodeTab[i]->xmlChildrenNode;
       while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"element")))  
	   {   int nIndexcri;
	       xmlNodePtr NodePtrChild;
		  
		   NodePtrChild=NodePtr->xmlChildrenNode;
           while(NodePtrChild != NULL)
	          {
	            if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"criterionID")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcri = find(criname.begin(), criname.end(), str) - criname.begin();
	            if(nIndexcri>=numcri){cout<<"The criterionID "<<str<<" is not valid, please check!";return NULL;}
				NodePtrChild=NodePtrChild->next;
				double temp =atof((const char*)xmlNodeGetContent(NodePtrChild)); 
                exprconswgt+= temp*x[nIndexcri];
			 
				}//reading criterionID
				 if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"variable")))
		      {
			    str =( const char*)xmlGetProp(NodePtrChild,BAD_CAST "id");
				if(str=="lamda")
				{
				NodePtrChild=NodePtrChild->next;
				double temp =atof((const char*)xmlNodeGetContent(NodePtrChild)); 
				exprconswgt+= temp*x[numcri];
				}
			 
				 }	//reading lamda
               NodePtrChild = NodePtrChild->next;
		   }

	   }//end of reading element
	   if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"rhs")))  
	   { 
	     double temp =atof((const char*)xmlNodeGetContent(NodePtr)); 
		 exprconswgt+= (-1)*temp; 
	   }//end of reading rhs
       if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"operator")))  
	   { 
	     str =(const char*)xmlNodeGetContent(NodePtr); 
		 if(str=="geq")	  {   x.add(IloNumVar(env)); con.add( exprconswgt- x[(x.getSize()-1)]== 0); con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);}
         if(str=="leq")	  {   x.add(IloNumVar(env));con.add( exprconswgt+ x[(x.getSize()-1)]== 0); con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);}
         if(str=="eq")	  {  con.add( exprconswgt== 0);}
	   }//end of reading operator

     
      NodePtr = NodePtr->next;  
		
	   }
	   
	  exprconswgt.clear(); 
	  cout<<"End of adding "<<i<<" th constraint of weights and lamda provided by DM..."<<endl; 
}

cout<<"End of adding constraints of weights and lamda provided by DM..."<<endl; 

//add objective function

   IloObjective obj=IloMaximize(env, x[numcri+1]);

   model.add(obj);

// constraints from the range of the alternatives
  
   int altindex;
   int lowcat;
   int upcat;
   IloExprArray  exprcons(env); 	 
	 
// add constraints for every alternative according to their assigned range
cout<<"Begin to add constraints for every alternative according to their assigned range..."<<endl; 

	   for (altindex=0;altindex<numalt;altindex++)
	   {
         lowcat=*(iiter12+altindex*2);
         upcat=*(iiter12+altindex*2+1);
		 Addcon(iiter13,lowcat,upcat,altindex,model,x,con,iiter7,iiter1,iiter2,iiter3,iiter4,cons,altname[altindex],d.begin());
	   }

    model.add(con);
    IloCplex cplex(model);  // define an object of IloCplex class
   	//cplex.exportModel("lpex474.lp"); // output the model
	
cout<<"End of adding constraints for every alternative according to their assigned range..."<<endl; 	
//solve the model

cout<<"Solving the model..."<<endl; 
   if ( !cplex.solve()||!cplex.getObjValue() ) 
   {
      consistencyresult=0;        
      throw(-1);
   }
 
  consistencyresult=1;

  
}// for try


  catch (IloException& e) {
      cerr << "Concert exception caught: " << e << endl;
   }
   catch (...) {
      cerr << "Unknown exception caught" << endl;
   }

cout<<"Begin to write bool result for consistency test..."<<endl; 
//write bool result for consistency test
//if inconsistency, output the constraints

   xmlDocPtr docnew_methodMessages=createXmlFile();
cout<<"The methodMessages file is created..."<<endl; 

   xmlNodePtr root_node=xmlDocGetRootElement(docnew_methodMessages);
   xmlNodePtr node, childnode,grandchild, node_criteriaLinearConstraints;
  
   node = xmlNewNode(NULL,BAD_CAST"methodMessages");
   xmlAddChild(root_node,node);
   childnode = xmlNewNode(NULL,BAD_CAST"message");
   grandchild = xmlNewNode(NULL,BAD_CAST"text");
   xmlAddChild(childnode,grandchild);
   xmlAddChild(node,childnode);

   if(consistencyresult==1)
   xmlAddChild(grandchild,xmlNewText(BAD_CAST"The preference information is consistent, you can infer weights and compute robust assignments by IRIS!"));
   else
   {
   cout<<"Begin output the constraints because of inconsistency..."<<endl; 

    xmlAddChild(grandchild,xmlNewText(BAD_CAST"The preference information is inconsistent, you can get suggestions by InconsistencyResolution!"));
   
   //if information is inconsistent, write inconsistent matrix to "inconsistency.xml"

    xmlDocPtr docnew_criteriaLinearConstraints=createXmlFile();
    cout<<"The criteriaLinearConstraints file is created..."<<endl; 
    xmlNodePtr root_node=xmlDocGetRootElement(docnew_criteriaLinearConstraints);
  // add the constraints from assignment examples
	
 cout<<"Add the constraints from assignment examples..."<<endl; 
 //use the same criteriaLinearConstraints tag in this file

	   node_criteriaLinearConstraints = xmlNewNode(NULL,BAD_CAST"criteriaLinearConstraints");   
	   xmlAddChild(root_node,node_criteriaLinearConstraints);
	  
	  for(int i=0;i<cons.size();i++)
	  {
	  childnode = xmlNewNode(NULL,BAD_CAST"constraint");
      xmlAddChild(node_criteriaLinearConstraints,childnode); 
cout<<"write the description of the constraint..."<<endl; 
    //write the description of the constraint
      node = xmlNewNode(NULL,BAD_CAST"name");      
	  xmlAddChild(node,xmlNewCDataBlock(docnew_criteriaLinearConstraints,BAD_CAST(cons[i].name.c_str()),cons[i].name.length()));//use cdata here because there are special symbols here
	  xmlAddChild(childnode,node);

	 cout<<"write  element..."<<endl; 
    //write  element
       for(int j=0;j<=numcri;j++)
		  {
		   node = xmlNewNode(NULL,BAD_CAST"element"); 		
		   if(j<numcri)
		   {
		   grandchild=xmlNewNode(NULL,BAD_CAST"criterionID");
		   xmlAddChild(grandchild,xmlNewText(BAD_CAST(criname[j].c_str())));
           xmlAddChild(node,grandchild);
           xmlAddChild(childnode,node);
		   }

		   else{
    cout<<"write lamda in element..."<<endl; 
	// write lamda in element
           grandchild=xmlNewNode(NULL,BAD_CAST"variable");
		   xmlSetProp(grandchild,BAD_CAST"mcdaConcept",BAD_CAST"cutting level");
		   xmlSetProp(grandchild,BAD_CAST"id",BAD_CAST"lamda");
           xmlAddChild(node,grandchild);
		   xmlAddChild(childnode,node);}
   
     cout<<"write the coefficient of the weights in the constraint..."<<endl; 
   //write the coefficient of the weights in the constraint	
		   grandchild = xmlNewNode(NULL,BAD_CAST"coefficient"); 		   
           xmlNodePtr sgrandchild=xmlNewNode(NULL,BAD_CAST"real");
		   char *buf= new char[100];
		   sprintf(buf,"%.4f",cons[i].consmatrix[j]); //keep 4 points of the number  
		   xmlAddChild(sgrandchild,xmlNewText(BAD_CAST(buf)));
           xmlAddChild(grandchild,sgrandchild);
		   xmlAddChild(node,grandchild);
           xmlAddChild(childnode,node);

	   }

  cout<<"write rhs..."<<endl; 
// write rhs

      node = xmlNewNode(NULL,BAD_CAST"rhs"); 	
      grandchild = xmlNewNode(NULL,BAD_CAST"real"); 		   
	  char *buf= new char[100];
	  sprintf(buf,"%.4f",cons[i].consmatrix[numcri+1]); 
	  xmlAddChild(grandchild,xmlNewText(BAD_CAST(buf)));
	  xmlAddChild(node,grandchild);
      xmlAddChild(childnode,node);
 //write the direction of the constraint
	    cout<<"write the direction of the constraint..."<<endl; 

	  node = xmlNewNode(NULL,BAD_CAST"operator"); 	
      xmlAddChild(node,xmlNewText(BAD_CAST"geq"));
      xmlAddChild(childnode,node);
//write confidence level
	   cout<<"write confidence level..."<<endl; 

       strXPath="/xmcda:XMCDA/alternativesAffectations/alternativeAffectation[alternativeID=\'";
       strXPath += cons[i].id;
	   strXPath += "\']/value"; 
	   NodeSetPtr=get_nodeset(doc_alternativesAffectations,strXPath.c_str());
	   node=xmlCopyNode(NodeSetPtr->nodeTab[0],1);
	   xmlAddChild(childnode,node);
       
	 }
   
	  cout<<"End of outputing the constraints because of inconsistency..."<<endl; 
      
   //copy the constraints of the original problem 
	

    cout<<"Copying the constraints of the original problem ..."<<endl; 
    NodeSetPtr = get_nodeset(doc_criteriaLinearConstraints,"/xmcda:XMCDA/criteriaLinearConstraints/constraint");
	

    for(int i=0;i<NodeSetPtr->nodeNr;i++)
    {
     childnode=xmlCopyNode(NodeSetPtr->nodeTab[i],1);
     xmlAddChild(node_criteriaLinearConstraints,childnode);
    }
      int writefile;
	  outputName = argv[2];
	  szDocName = outputName;
	  szDocName+= "/criteriaLinearConstraints.xml";
	  writefile=xmlSaveFileEnc(szDocName.c_str(),docnew_criteriaLinearConstraints,"UTF-8");
	  //writefile=saveXmlFile(docnew_criteriaLinearConstraints,szDocName.c_str());
	  if(writefile==-1)
	  {   cout<<"Failed to write criteriaLinearConstraints to xml file!"<<endl;
		 return NULL;}
	    else cout<<"The criteriaLinearConstraints file is saved..."<<endl; 


  }
  
     cout<<"End of writing bool result for consistency test..."<<endl; 
 
	 int writefile;
	 outputName = argv[2];
	 szDocName = outputName;
	 szDocName+= "/methodMessages.xml";
	 writefile=xmlSaveFileEnc(szDocName.c_str(),docnew_methodMessages,"UTF-8");
	 if(writefile==-1)
	 {   cout<<"Failed to write methodMessages to xml file!"<<endl;
		 return NULL;}
	 else cout<<"The methodMessages file is saved..."<<endl; 

     closeXmlFile(doc_alternatives);	
	 closeXmlFile(doc_alternativesAffectations);
	 closeXmlFile(doc_performanceTable);
	 closeXmlFile(doc_criteria);
	 closeXmlFile(doc_categoriesProfiles);
	 closeXmlFile(doc_criteriaLinearConstraints);
	 closeXmlFile(doc_categoriesComparisons);
	 closeXmlFile(doc_categories);
	 //closeXmlFile(docnew);
	 env.end();	
	 cout<<"The program is ended successfully!"<<endl; 
	 return 0;

}
