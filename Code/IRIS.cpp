/********************************************************************
    Note: two parameters should be provided as input for the command line
	      the first one is the path of the input
		  the second one is the path of the output
	created:201003	
	author:	ZHENG JUN	
	purpose: For a sorting problem, use ELECTRE TRI method to infer weights and lamda that best
	         matches the preference of the DM, to assign every alternative to the predefined 
			 category and to compute robust assignments.
*********************************************************************/
# include "Parser.h"
# include <iconv.h>
# include <libxml/parser.h>
# include <libxml/xpath.h>
# include <iostream>
# include <vector>
# include<algorithm>
# include <ilcplex/ilocplex.h>
# include <sstream>

using namespace std;


//#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.VC80.CRT' version='8.0.50608.0' processorArchitecture='X86' publicKeyToken='1fc8b3b9a1e18e3b' language='*'\"")

int numcri; 
int numcat;
int numalt;
bool consistencyresult;

 class constraint
{

public:	
   string name;
  
   vector<double> consmatrix;

   string id;

   constraint(string nameofcon, vector< double > con,string alternativename)
   {
    name=nameofcon;

    consmatrix=con;

	id=alternativename;


   }
    constraint()
   {
    

   }
  void getvalue(string nameofcon, vector< double > con)
   {
	   name=nameofcon;

       consmatrix=con;

   }
  
};
void writevector(vector< constraint > &cons,vector< double > &weights,ostringstream &oss,string alternativename)
{

//define the vector for the weights of one constraint

vector< double > weightszero(numcri+2, 0);
string dcr;
dcr=oss.str();
constraint con1(dcr,weights,alternativename);
cons.push_back(con1);
weights=weightszero;
oss.str("");


}
void ComputeCI(vector< int >::iterator direction,vector< double >::iterator c,int e,vector< double >::iterator eval,vector< double >::iterator g,vector< double >::iterator p, vector< double >::iterator q);


void Addcon(vector< int >::iterator direction,int lowcat, int upcat, int exampindex, IloModel model, IloNumVarArray x, IloRangeArray con,vector< double >::iterator c,vector< double >::iterator eval,
			vector< double >::iterator g,vector< double >::iterator q,vector< double >::iterator p,vector< constraint > &cons, string alternativename,vector< double >::iterator d)
{
 // write to constraint vectors
vector< double > weights(numcri+2);
vector< double > weights1(numcri+2);
ostringstream oss;

//////////////////
	
IloEnv env = model.getEnv();

// compute partial concordance index of the assigned alternative
        
 ComputeCI(direction,c,exampindex,eval,g,p,q);

 //add constaints

 IloExpr expr1(env);

 IloExpr expr2(env);

int index;

if ((lowcat==1)||(upcat==numcat))
{
if ((lowcat==1)&&(upcat!=numcat))
{for (index = 0; index < numcri; index++)
{
expr2 += (*(c+(upcat-1)*numcri+index)) * x[index];  
 weights[index]= -1*(*(c+(upcat-1)*numcri+index));
 }

  expr2= expr2*(*(d+(exampindex*(numcat-1)+upcat-1)));// add veto
  expr2 +=- x[numcri];  
  weights[numcri]=1;
  weights[numcri+1]=0.0001;//sigma(a, bh)<lamda strictly
  oss<<"C["<<alternativename<<"]<="<<upcat;// change the form of name to make it clear
  writevector(cons,weights,oss,alternativename);

  x.add(IloNumVar(env));
  con.add(-1*expr2 - x[(x.getSize()-1)]== 0);
  con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);
}

if((lowcat!=1)&&(upcat==numcat))
  {for (index = 0; index < numcri; index++)
{
	expr1 += (*(c+(lowcat-2)*numcri+index)) * x[index];
    weights[index]= (*(c+(lowcat-2)*numcri+index));
}
   expr1= expr1*(*(d+(exampindex*(numcat-1)+lowcat-2)));  // add veto

   expr1 += - x[numcri];
   weights[numcri]=-1;
   oss<<"C["<<alternativename<<"]>="<<lowcat;
   writevector(cons,weights,oss,alternativename);

   x.add(IloNumVar(env));
   con.add(expr1- x[(x.getSize()-1)]== 0);
   con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);
  }
}
else
{ 
   for (index = 0; index < numcri; index++)
   { 
	 expr1 += (*(c+(lowcat-2)*numcri+index)) * x[index];
	  weights[index]= (*(c+(lowcat-2)*numcri+index));
	 expr2 += (*(c+(upcat-1)*numcri+index)) * x[index]; 
	  weights1[index]= -1*(*(c+(upcat-1)*numcri+index));
      }

    expr1= expr1*(*(d+(exampindex*(numcat-1)+lowcat-2)));  // add veto
    expr2= expr2*(*(d+(exampindex*(numcat-1)+upcat-1)));// add veto

    x.add(IloNumVar(env));
    expr1 +=- x[numcri];
    weights[numcri]=-1;
	oss<<"C["<<alternativename<<"]>="<<lowcat;
	writevector(cons,weights,oss,alternativename);

    con.add(expr1- x[(x.getSize()-1)]== 0);
    con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);

   x.add(IloNumVar(env));
   expr2 +=- x[numcri]; 
   weights1[numcri]=1;
   weights1[numcri+1]=0.0001;//sigma(a, bh)<lamda strictly
   oss<<"C["<<alternativename<<"]<="<<upcat;
   writevector(cons,weights1,oss,alternativename);

   con.add(-1*expr2 - x[(x.getSize()-1)]== 0);
   con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);




}// for else



}// for end

void Judgecat(int m, vector< int >::iterator result, vector< double >::iterator cc, double cut)
{
  int i;

//pessimistic
   for(i=(numcat-2);i>=0;)
  {
	 // cout<<"cc"<<(m*(numcat-1)+i)<<"is "<<(*(cc+m*(numcat-1)+i))<<endl;
	//  cout<<"lamda is "<<(*cut)<<endl;
	if(fabs(*(cc+m*(numcat-1)+i)-cut)<1e-6) {*(result+m)=i+2;   break;  }//firstly judge if cc is equal with lamda(because computer will judge two equal data of double types unequal)if cc is equal to lamda, the category is determined
	else if(*(cc+m*(numcat-1)+i)<cut)//if not equal, determin which is bigger
	{ if(i==0) { *(result+m)=1;}
		  i--;}
	  else {*(result+m)=i+2;   break;  }//if cc is greater than lamda, the category is determined
	  
		  
  }

}
 void ComputeCI(vector< int >::iterator direction,vector< double >::iterator c,int e,vector< double >::iterator eval,vector< double >::iterator g,vector< double >::iterator p, vector< double >::iterator q)
{
    
	int i;
	int j;
//judge the direction of the criteria	
for(i=0;i<(numcat-1);i++)
	for(j=0;j<numcri;j++)
	{    
	    if(*(direction+j)==-1)
		{if(*(p+j)==0)
	   {if(*(g+i*numcri+j)<*(eval+e*numcri+j))   *(c+i*numcri+j)=0;
	    	else *(c+i*numcri+j)=1;		   }
	     else
		 *(c+i*numcri+j)=(*(p+j)-min(*(eval+e*numcri+j)-*(g+i*numcri+j), *(p+j)))/(*(p+j)-min(*(eval+e*numcri+j)-*(g+i*numcri+j), *(q+j)));}
	

		 else
		{ 
		if(*(p+j)==0)
		{if(*(g+i*numcri+j)>*(eval+e*numcri+j))  *(c+i*numcri+j)=0;
	    	else *(c+i*numcri+j)=1;
		}
	     else
		 *(c+i*numcri+j)=(*(p+j)-min(*(g+i*numcri+j)-*(eval+e*numcri+j), *(p+j)))/(*(p+j)-min(*(g+i*numcri+j)-*(eval+e*numcri+j), *(q+j)));
		
		
		}
}

	}

 
void ComputeCCI(int e,vector< double >::iterator cc,vector< double >::iterator w, vector< double >::iterator c,vector< double >::iterator d)
{

int i;
int j;
for(i=0;i<(numcat-1);i++)
  for(j=0;j<numcri;j++)
	  *(cc+e*(numcat-1)+i)=0;
i=0;
j=0;
for(i=0;i<(numcat-1);i++)
{ for(j=0;j<numcri;j++)
  {*(cc+e*(numcat-1)+i)=(*(w+j))*(*(c+i*numcri+j))+(*(cc+e*(numcat-1)+i));
    //cout<<"w"<<j<<"is:"<<*(w+j)<<endl;
    //cout<<"c"<<j<<"is:"<<*(c+i*numcri+j)<<endl;
	//cout<<"cc"<<j<<"is:"<<*(cc+e*(numcat-1)+i)<<endl;
  }
 *(cc+e*(numcat-1)+i)=(*(cc+e*(numcat-1)+i))*(*(d+e*(numcat-1)+i));
 //cout<<"cc"<<j<<"is:"<<*(cc+e*(numcat-1)+i)<<endl;
 //cout<<"d"<<j<<"is:"<<*(d+e*(numcat-1)+i)<<endl;
}
}
void Delcon(int categindex, IloModel model, IloRangeArray con,int numcat, IloNumVarArray x)
{
 IloEnv env = model.getEnv();

 int size=con.getSize();
 int size_x=x.getSize();

 model.remove(con);

 //delete constraints and the related varibles
 if((categindex==1)||(categindex==numcat)) { con.remove ( (size-2),2); x.remove( (size_x-1),1);}
 else   {con.remove ( (size-4),4);x.remove( (size_x-2),2);}
}

int main(int argc, char* argv[])
{	
	
xmlKeepBlanksDefault (0);
 
// evaluation matrix
 vector <double> eval;

//limits of categories matrix
 vector <double> g;
 
 //indiffernce thresholds matrix
 vector <double> q;

 // preference thresholds matrix
 vector <double> p;
 
 //veto matrix
vector <double> v;

//cut limit
double cut;

//define lower and upper category matrix
vector <int> range;

//define direction of preference matrix
vector <int> direction;

//define the constraints of weights provided by DM
vector <double> conswgt;

//define the vector for names of criteria
vector< string > criname;

//define the vector for names of alternatives
vector< string > altname;
//define the vector for names of criteria
vector< string > catname;

string inputName, outputName,szDocName;
xmlNodeSetPtr NodeSetPtr;
 //get the path of document to be readed from command line

//cout<<"argc"<<argc<<endl;

    if (argc <= 2) 

    {
      cout<<"The input parameters are incomplete!"<<endl;

       return(0);

    }

	inputName = argv[1];	

	szDocName =  inputName + "/alternatives.xml";
	xmlDocPtr doc_alternatives = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_alternatives!=NULL) cout<<"The alternatives file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/categories.xml";
	xmlDocPtr doc_categories = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categories!=NULL) cout<<"The categories file has been successfully opened!"<<endl;
	else   return NULL;

	szDocName =  inputName + "/categoriesComparisons.xml";
	xmlDocPtr doc_categoriesComparisons = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categoriesComparisons!=NULL) cout<<"The categoriesComparisons file has been successfully opened!"<<endl;
    else   return NULL;
    
	szDocName =  inputName + "/criteriaLinearConstraints.xml";
	xmlDocPtr doc_criteriaLinearConstraints = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_criteriaLinearConstraints!=NULL) cout<<"The criteriaLinearConstraints file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/categoriesProfiles.xml";
	xmlDocPtr doc_categoriesProfiles = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_categoriesProfiles!=NULL) cout<<"The categoriesProfiles file has been successfully opened!"<<endl;
	else  return NULL;

	szDocName =  inputName + "/criteria.xml";
	xmlDocPtr doc_criteria = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_criteria!=NULL) cout<<"The criteria file has been successfully opened!"<<endl;
    else  return NULL;

	szDocName =  inputName + "/performanceTable.xml";
	xmlDocPtr doc_performanceTable = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_performanceTable!=NULL)	cout<<"The performanceTable file has been successfully opened!"<<endl;
	else   return NULL;

	szDocName =  inputName + "/alternativesAffectations.xml";
	xmlDocPtr doc_alternativesAffectations = xmlReadFile(szDocName.c_str(),NULL,XML_PARSE_RECOVER);
	if(doc_alternativesAffectations!=NULL)	cout<<"The alternativesAffectations file has been successfully opened!"<<endl;
	else cout<<"No assignment example!"<<endl;             

//read criteria related data
cout<<"Begin to read criteria related data..."<<endl;
	NodeSetPtr = get_nodeset(doc_criteria,"/xmcda:XMCDA/criteria/criterion");
	if (NodeSetPtr == NULL) 
	{
		return NULL;  //if the set of criteria is null, the program is ended
	}

	numcri= NodeSetPtr->nodeNr;
	
   for (int i=0; i<numcri; ++i)
  {
	//get criterion name
     string strXPath,temp;
	 double tempdata;

	 xmlChar* szAttr = xmlGetProp(NodeSetPtr->nodeTab[i],BAD_CAST "id");

	 if(szAttr==NULL)	 return NULL;

	 criname.push_back(( char *)szAttr);

	 //criterion preference direction information 
	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
     strXPath += "\']/scale";
	 temp=getXmlString(doc_criteria,strXPath.c_str());
	 
	 if(temp=="NULL")	 return NULL;     

     if(temp=="max")   direction.push_back(1);
	 else   direction.push_back(-1);

cout<<"End of reading criterion preference direction information..."<<endl;

cout<<"Begin to read thresholds information..."<<endl;

	 //read thresholds
	 strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/thresholds/threshold[@mcdaConcept='pref']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());

      if(tempdata==NULL)	 return NULL;    

	 p.push_back(tempdata);

	  strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
   	 strXPath += "\']/thresholds/threshold[@mcdaConcept='ind']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());
	 q.push_back(tempdata);
	 
	  strXPath="/xmcda:XMCDA/criteria/criterion[@id=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/thresholds/threshold[@mcdaConcept='veto']/constant";
	 tempdata=getXmlDouble(doc_criteria,strXPath.c_str());
	 if(tempdata==0) tempdata=1000;
	 v.push_back(tempdata);
	 cout<<"End of reading thresholds information..."<<endl;
	
   }
  
  //end of read criteria related data

cout<<"Begin to getting alternative evaluation matrix..."<<endl;

//read alternative evaluation matrix

    NodeSetPtr = get_nodeset(doc_alternatives,"/xmcda:XMCDA/alternatives/alternative[active='true']");
	if (NodeSetPtr == NULL) 
	{
		return NULL;  //if the set of criteria is null, the program is ended
	}

	numalt= NodeSetPtr->nodeNr; 
	
   for (int i=0; i<numalt; ++i)
   { 
   
     //get alternative name
     string strXPath,temp;
	 double tempdata;

	 xmlChar* szAttr = xmlGetProp(NodeSetPtr->nodeTab[i],BAD_CAST "id");
	 altname.push_back(( char *)szAttr);

     //get performance of every alternative 
	 for (int j=0; j<numcri; ++j)
	 {		 
	 strXPath="/xmcda:XMCDA/performanceTable/alternativePerformances[alternativeID=\'";
     strXPath += ( char *)szAttr;
  	 strXPath += "\']/performance[criterionID=\'";
	 strXPath += criname[j];
	 strXPath += "\']/value";
	 tempdata=getXmlDouble(doc_performanceTable,strXPath.c_str());
	  if(tempdata==1e6)	 return NULL;     
	 eval.push_back(tempdata);   
	 }
   
   
   }
   
 cout<<"End of getting alternative evaluation matrix..."<<endl;  
// end of read alternative evaluation matrix

//read profiles data
cout<<"Begin to read profiles data..."<<endl;

  string strXPath,str;
  xmlNodePtr NodePtr;  

  NodeSetPtr = get_nodeset(doc_categories,"/xmcda:XMCDA/categories/category");
  if (NodeSetPtr == NULL)		return NULL;  //if the set of criteria is null, the program is ended
	
  numcat= NodeSetPtr->nodeNr; 
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial";
  NodeSetPtr = get_nodeset(doc_categoriesComparisons,strXPath.c_str());
  if (NodeSetPtr == NULL)		return NULL;
  str=getXmlString(doc_categoriesComparisons,strXPath.c_str());


while(NodeSetPtr!=NULL)
{ 
  NodePtr=NodeSetPtr->nodeTab[0]->parent;
  NodePtr=NodePtr->xmlChildrenNode;

  while(NodePtr != NULL)
    { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"initial")))       
           str = (const char*)xmlNodeGetContent(NodePtr);     
      NodePtr = NodePtr->next;   
   }
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/terminal[categoryID=\'";
  strXPath += str;
  strXPath += "\']";
  
  //use the code in get_nodeset function, because we don't want to output the error message here
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result;
  context = xmlXPathNewContext(doc_categoriesComparisons);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
   if (result == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result->nodesetval)) 
	{
		xmlXPathFreeObject(result);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result->nodesetval;
}
catname.push_back(str);//find the lowest category

//write category name in order 
strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial[categoryID=\'";
strXPath += str;
strXPath += "\']";
NodeSetPtr = get_nodeset(doc_categoriesComparisons,strXPath.c_str()); 
while(NodeSetPtr!=NULL)
{ 
  NodePtr=NodeSetPtr->nodeTab[0]->parent;
  NodePtr=NodePtr->xmlChildrenNode;

  while(NodePtr != NULL)
    { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"terminal")))       
         { str = (const char*)xmlNodeGetContent(NodePtr); 
           catname.push_back(str);
         }
      NodePtr = NodePtr->next;   
   }
  strXPath="/xmcda:XMCDA/categoriesComparisons/pairs/pair/initial[categoryID=\'";
  strXPath += str;
  strXPath += "\']";
  
  //use the code in get_nodeset function, because we don't want to output the error message here
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result;
  context = xmlXPathNewContext(doc_categoriesComparisons);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
   if (result == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result->nodesetval)) 
	{
		xmlXPathFreeObject(result);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result->nodesetval;
}

//write category name in order

//read performance of category limits
for (int i=0; i<(numcat-1); ++i)
{

  strXPath="/xmcda:XMCDA/categoriesProfiles/categoryProfile/limits/lowerCategory[categoryID=\'";	
  strXPath += catname[i];
  strXPath += "\']";
  NodeSetPtr = get_nodeset(doc_categoriesProfiles,strXPath.c_str());
  NodePtr=NodeSetPtr->nodeTab[0]->parent->parent;
  NodePtr=NodePtr->xmlChildrenNode;
 
  while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"alternativeID")))       
        str = (const char*)xmlNodeGetContent(NodePtr);         
        NodePtr = NodePtr->next;   
   }
   for (int j=0; j<numcri; ++j)
	 {double tempdata;		 
	 strXPath="/xmcda:XMCDA/performanceTable/alternativePerformances[alternativeID=\'";
     strXPath += str;
  	 strXPath += "\']/performance[criterionID=\'";
	 strXPath += criname[j];
	 strXPath += "\']/value";
	 tempdata=getXmlDouble(doc_performanceTable,strXPath.c_str());
	 g.push_back(tempdata);
	 }


}

cout<<"End of reading profiles data..."<<endl;
//end of read performance of category limits

//initialize range matrix(1,numcat)
cout<<"Initializing range matrix..."<<endl;
for (int i=0;i<numalt;i++)
 for (int j=0;j<2;j++)
 {
	 if (j==0) range.push_back(1);
 else  range.push_back(numcat);
 }

 //get iterator for the matrix which have been initialized

vector< double >::iterator iiter1 = eval.begin();

 vector< double >::iterator iiter2 = g.begin();

 vector< double >::iterator iiter3 = q.begin();
 
 vector< double >::iterator iiter4 = p.begin();

 vector< double >::iterator iiter11 = v.begin();

vector< int>::iterator iiter12 = range.begin();

vector< int >::iterator iiter13 = direction.begin();

vector< string >::iterator iiter_criname= criname.begin();

vector< string >::iterator iiter_altname= altname.begin();;

vector< string >::iterator iiter_catname= catname.begin();
cout<<"Defining matrix..."<<endl;
 // weights
vector <double> w(1*numcri);
vector< double >::iterator iiter5 = w.begin();

//define partial concordance index matrix
vector <double> c((numcat-1)*numcri);
vector< double >::iterator iiter7 = c.begin();

//define disconcordance index matrix
vector <double> d(numalt*(numcat-1));

//define comprehensive concordance index matrix
vector <double> cc(numalt*(numcat-1));
vector< double >::iterator iiter8 = cc.begin();

//define result matrix
vector <int> result(1*numalt);
vector< int >::iterator iiter9 = result.begin();

//define robust conclusion matrix
vector <int> robust(numalt*numcat);
vector< int >::iterator iiter10 = robust.begin();

//define the vector for the weights of one constraint
vector< double > weights(numcri+2);
vector< double > weightszero(numcri+2, 0);

//compute discordance index
cout<<"Begin to compute discordance index..."<<endl; 
 
vector< double > dj(numcri,0);
for(int e=0;e<numalt;e++)
for(int i=0;i<(numcat-1);i++)
  {    int discordancej=1;
	for(int j=0;j<numcri;j++)
	{  
	    if(direction[j]==-1)//judge the direction of the criteria
		{
		dj[j]=(v[j]-min(eval[e*numcri+j]-g[i*numcri+j], v[j]))/(v[j]-min(eval[e*numcri+j]-g[i*numcri+j], (p[j]+0.75*(v[j]-p[j]))));
		dj[j]= 1-dj[j];
		}
	
		 else
		{ 
		dj[j]=(v[j]-min(g[i*numcri+j]-eval[e*numcri+j], v[j]))/(v[j]-min(g[i*numcri+j]-eval[e*numcri+j], (p[j]+0.75*(v[j]-p[j]))));
		dj[j]= 1-dj[j];
		}
       	discordancej=discordancej*(1-dj[j]);

	}
	*(d.begin()+e*(numcat-1)+i)=discordancej;
}
 
 
cout<<"End of computing discordance index..."<<endl;  

//read assignment examples provided by DM
//use the code in get_nodeset function, because we don't want to output the error message here

cout<<"Begin to read assignment examples provided by DM..."<<endl; 

  strXPath= "/xmcda:XMCDA/alternativesAffectations/alternativeAffectation";
  xmlXPathContextPtr context;	
  xmlXPathObjectPtr result0;
  context = xmlXPathNewContext(doc_alternativesAffectations);
  if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }
   result0 = xmlXPathEvalExpression( (xmlChar *)(strXPath.c_str()), context);
   xmlXPathFreeContext(context);
  if (result0 == NULL) 		NodeSetPtr= NULL; 	
   else if (xmlXPathNodeSetIsEmpty(result0->nodesetval)) 
	{
		xmlXPathFreeObject(result0);
		NodeSetPtr= NULL;
	}
	else	NodeSetPtr = result0->nodesetval;
 

 if (NodeSetPtr != NULL)  //if the set is null, it means that there is no assignment example
	{
		for (int i=0;i<NodeSetPtr->nodeNr;i++)
   {  int nIndexalt,nIndexcat;
	   NodePtr=NodeSetPtr->nodeTab[i]->xmlChildrenNode;
       while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"alternativeID")))       
	 { str = (const char*)xmlNodeGetContent(NodePtr); 	 
	   nIndexalt = find(altname.begin(), altname.end(), str) - altname.begin();
	  // cout<<nIndexalt<<endl;
	   if(nIndexalt>=numalt){cout<<"Alternative "<<str<<" is not a valid alternative, please check!"; return NULL;}
	 }
	   if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"categoriesInterval")))       
	   { xmlNodePtr NodePtrChild;
		  NodePtrChild=NodePtr->xmlChildrenNode;
           while(NodePtrChild != NULL)
	          {
	            if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"lowerBound")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcat = find(catname.begin(), catname.end(), str) - catname.begin();
	    		if(nIndexcat>=numcat){cout<<"The lower bound of "<<altname[nIndexalt]<<" is not a valid category bound, please check!";return NULL;}
			    range[nIndexalt*2]=nIndexcat+1;

		      }
	           if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"upperBound")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcat = find(catname.begin(), catname.end(), str) - catname.begin();
	         	if(nIndexcat>=numcat){cout<<"The upper bound of "<<altname[nIndexalt]<<" is not a valid category bound, please check!";return NULL;}
			    range[nIndexalt*2+1]=nIndexcat+1;
		       }
               NodePtrChild = NodePtrChild->next;
		      }
	 }
     NodePtr = NodePtr->next;  
		
	   }
   
	 }
 }
//end of read alternative range
cout<<"End of reading read assignment examples provided by DM..."<<endl; 


//define the string for the description of one constraint
string dcr;
//define the vector for many constraints
vector <constraint> cons;


IloEnv   env;
try{
    IloModel model(env); //define a model    
    IloNumVarArray x(env); //define varibles
    IloRangeArray con(env);  //define constaints
cout<<"Add weights varibles..."<<endl;  
//add weights varibles 
string filename;
ostringstream oss;
	for(int count=0;count<numcri;count++)
{ 
	x.add(IloNumVar(env));    
	oss<<"w"<<count;
	filename=oss.str();
	x[(x.getSize()-1)].setName(filename.c_str());
	oss.str(" ");
	
}
//add lamda varible
cout<<"Add lamda and objective function..."<<endl; 
   x.add(IloNumVar(env));
   x[numcri].setName("lamda");
//add obj varible
   x.add(IloNumVar(env));        
   x[numcri+1].setName("minerr");

//add constraints of weights and lamda provided by DM
cout<<"Begin to add constraints of weights and lamda provided by DM..."<<endl; 
 IloExpr exprconswgt(env);
 NodeSetPtr = get_nodeset(doc_criteriaLinearConstraints,"/xmcda:XMCDA/criteriaLinearConstraints/constraint");
 if (NodeSetPtr == NULL) 		return NULL;  //if the set of criteria is null, the program is ended
	

	 for (int i=0;i<NodeSetPtr->nodeNr;i++)
	 {
	   
	   NodePtr=NodeSetPtr->nodeTab[i]->xmlChildrenNode;
       while(NodePtr != NULL)
	 { if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"element")))  
	   {   int nIndexcri;
	       xmlNodePtr NodePtrChild;
		  
		   NodePtrChild=NodePtr->xmlChildrenNode;
           while(NodePtrChild != NULL)
	          {
	            if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"criterionID")))
		      {
			    str = (const char*)xmlNodeGetContent(NodePtrChild); 
				nIndexcri = find(criname.begin(), criname.end(), str) - criname.begin();
	            if(nIndexcri>=numcri){cout<<"The criterionID "<<str<<" is not valid, please check!";return NULL;}
				NodePtrChild=NodePtrChild->next;
				double temp =atof((const char*)xmlNodeGetContent(NodePtrChild)); 
                exprconswgt+= temp*x[nIndexcri];
			 
				}//reading criterionID
				 if ((!xmlStrcmp(NodePtrChild->name, (const xmlChar *)"variable")))
		      {
			    str =( const char*)xmlGetProp(NodePtrChild,BAD_CAST "id");
				if(str=="lamda")
				{
				NodePtrChild=NodePtrChild->next;
				double temp =atof((const char*)xmlNodeGetContent(NodePtrChild)); 
				exprconswgt+= temp*x[numcri];
				}
			 
				 }	//reading lamda
               NodePtrChild = NodePtrChild->next;
		   }

	   }//end of reading element
	   if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"rhs")))  
	   { 
	     double temp =atof((const char*)xmlNodeGetContent(NodePtr)); 
		 exprconswgt+= (-1)*temp; 
	   }//end of reading rhs
       if ((!xmlStrcmp(NodePtr->name, (const xmlChar *)"operator")))  
	   { 
	     str =(const char*)xmlNodeGetContent(NodePtr); 
		 if(str=="geq")	  {   x.add(IloNumVar(env)); con.add( exprconswgt- x[(x.getSize()-1)]== 0); con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);}
         if(str=="leq")	  {   x.add(IloNumVar(env));con.add( exprconswgt+ x[(x.getSize()-1)]== 0); con.add(x[(x.getSize()-1)]-x[numcri+1]>= 0);}
         if(str=="eq")	  {  con.add( exprconswgt== 0);}
	   }//end of reading operator

     
      NodePtr = NodePtr->next;  
		
	   }
	   
	  exprconswgt.clear(); 
}
cout<<"End of adding constraints of weights and lamda provided by DM..."<<endl; 

//add objective function

   IloObjective obj=IloMaximize(env, x[numcri+1]);

   model.add(obj);

// constraints from the range of the alternatives
  
   int altindex;
   int lowcat;
   int upcat;
   IloExprArray  exprcons(env); 	 
cout<<"Begin to add constraints for every alternative according to their assigned range..."<<endl; 	 
// add constraints for every alternative according to their assigned range
	   for (altindex=0;altindex<numalt;altindex++)
	   {
         lowcat=*(iiter12+altindex*2);
         upcat=*(iiter12+altindex*2+1);
		 if((lowcat!=1)||(upcat!=numcat))
		 { Addcon(iiter13,lowcat,upcat,altindex,model,x,con,iiter7,iiter1,iiter2,iiter3,iiter4,cons,altname[altindex],d.begin());
	      cout<<"The constraint from assignment"<<altname[altindex]<<"is added!"<<endl;
		 }
	   
	   }

    model.add(con);
    IloCplex cplex(model);  // define an object of IloCplex class
   
cout<<"End of adding constraints for every alternative according to their assigned range..."<<endl; 
//solve the model

cout<<"Solving the model..."<<endl; 
   if ( !cplex.solve()||!cplex.getObjValue() ) 
   {
      consistencyresult=0;        
      throw(-1);
   }
 
   consistencyresult=1;
   
   IloNumArray vals(env);
  // env.out() << "Solution status = " << cplex.getStatus() << endl;
   //env.out() << "Solution value  = " << cplex.getObjValue() << endl;
   cplex.getValues(x,vals);
  // env.out() << "Values        = " << vals << endl;

//write the weights to the weights array and lamda variable

cout<<"writing the weights to the weights array and lamda variable..."<<endl; 
   oss<<"weights [";
    for (int s=0; s<numcri; s++)
           {  
            *(iiter5+s)=vals[s];
			oss<<vals[s]<<" ";
		   }
          cut=vals[numcri];
		
   oss<<"] lamda "<<vals[numcri]<<endl;


  //compute partial concordance index, comprehensive concordance index and then judge catogries
cout<<"Computing partial concordance index, comprehensive concordance index and then judge catogries..."<<endl; 
     for (int altindex=0;altindex<numalt;altindex++)
	{ 

    ComputeCI(iiter13,iiter7,altindex,iiter1,iiter2,iiter4,iiter3);

	ComputeCCI(altindex,iiter8,iiter5,iiter7,d.begin());

	Judgecat(altindex,iiter9,iiter8,cut);

	cout<<altname[altindex]<<" is sorted!"<<endl;
	
		
	}
 
  
   
//write robust assignment to xml doucment

//create methodmessages element, write central weights and lamda to message
cout<<"create methodmessages file, write central weights and lamda to message..."<<endl; 
   xmlNodePtr root_node;
   xmlDocPtr docnew_methodmessages=createXmlFile();
   root_node=xmlDocGetRootElement(docnew_methodmessages);
   xmlNodePtr node, childnode,grandchild;
  
   node = xmlNewNode(NULL,BAD_CAST"methodMessages");
   xmlAddChild(root_node,node);
   childnode = xmlNewNode(NULL,BAD_CAST"message");
   grandchild = xmlNewNode(NULL,BAD_CAST"text");
   xmlAddChild(childnode,grandchild);
   xmlAddChild(node,childnode);
   xmlAddChild(grandchild,xmlNewText(BAD_CAST(oss.str().c_str())));
   oss.str("");

   int writefile;
   outputName = argv[2];
   szDocName = outputName;
   szDocName+= "/methodMessages_weights.xml";
   writefile=xmlSaveFileEnc(szDocName.c_str(),docnew_methodmessages,"UTF-8");
   if(writefile==-1)
   {   cout<<"Failed to write methodMessages to xml file!"<<endl;
		 return NULL;}
	   else cout<<"The methodMessages file is saved..."<<endl; 

cout<<"create alternativesAffectations file for sorting result..."<<endl; 
   xmlDocPtr docnew_alternativesAffectations1=createXmlFile();
   root_node=xmlDocGetRootElement(docnew_alternativesAffectations1);

cout<<"create alternativesAffectations element for sorting result..."<<endl; 

//   create alternativesAffectations element for sorting result

   
   node = xmlNewNode(NULL,BAD_CAST"alternativesAffectations");
   xmlAddChild(root_node,node);
   xmlSetProp(node,BAD_CAST"mcdaConcept",BAD_CAST"Sorting result based on parameter values that best match the provided information");


   for ( int i=0; i<numalt; i++)
   {
   //create alternativesAffectation element 
   childnode = xmlNewNode(NULL,BAD_CAST"alternativeAffectation");
   xmlAddChild(node,childnode);
   
   	//append alternativeID child node
   grandchild = xmlNewNode(NULL,BAD_CAST"alternativeID");
   xmlAddChild(childnode,grandchild);
   xmlAddChild(grandchild,xmlNewText(BAD_CAST((*(iiter_altname+i)).c_str())));

   //append categoryID child node
   grandchild = xmlNewNode(NULL,BAD_CAST"categoryID");
   xmlAddChild(childnode,grandchild);
   int categoryid=(*(iiter9+i));
   xmlAddChild(grandchild,xmlNewText(BAD_CAST((*(iiter_catname+categoryid-1)).c_str())));

   }

   
   szDocName = outputName;
   szDocName+= "/alternativesAffectations_sorting.xml";
   writefile=xmlSaveFileEnc(szDocName.c_str(),docnew_alternativesAffectations1,"UTF-8");
   if(writefile==-1)
   {   cout<<"Failed to write alternativesAffectations of sorting result to xml file!"<<endl;
		 return NULL;}
	   else cout<<"The docnew_alternativesAffectations of sorting result file is saved..."<<endl; 
  closeXmlFile(docnew_alternativesAffectations1);


cout<<"create alternativesAffectations file for robust assignments..."<<endl;

   xmlDocPtr docnew_alternativesAffectations2=createXmlFile();
   root_node=xmlDocGetRootElement(docnew_alternativesAffectations2);
cout<<"create alternativesAffectations element for robust assignments..."<<endl; 
//   create alternativesAffectations element for robust assignments
   node = xmlNewNode(NULL,BAD_CAST"alternativesAffectations");
   xmlAddChild(root_node,node);
   xmlSetProp(node,BAD_CAST"mcdaConcept",BAD_CAST"Robust assignments");

int index1,index2;
for ( index1 = 0; index1 < numalt; index1 ++)
{ 
//create alternativesAffectation element   
   childnode = xmlNewNode(NULL,BAD_CAST"alternativeAffectation");
   xmlAddChild(node,childnode);

//append alternativeID child node
   grandchild = xmlNewNode(NULL,BAD_CAST"alternativeID");
   xmlAddChild(childnode,grandchild);
   xmlAddChild(grandchild,xmlNewText(BAD_CAST((*(iiter_altname+index1)).c_str())));

//append categoriesSet child node
   grandchild = xmlNewNode(NULL,BAD_CAST"categoriesSet");
   xmlAddChild(childnode,grandchild);
   childnode=grandchild;//now childnode is categoriesSet


   for (index2 = 1; index2 <= numcat; index2 ++)
   {
      
	  Addcon(iiter13,index2,index2,index1,model,x,con,iiter7,iiter1,iiter2,iiter3,iiter4,cons,altname[index1],d.begin());	 
	  cout<<"Computing Robust Assignment:Constaints for assigning "<<altname[index1]<<" to "<<catname[index2-1]<<" is added!"<<endl;
	  model.add(con);    
	  //cplex.exportModel("lpex2.lp");

     //judge the status of the solution of related linear program and check if there exsists the situation of "hole"
	
     if ( cplex.solve() && cplex.getObjValue()) 
    		 
		 (*(iiter10+index1*numcat+index2-1))=1;
     else (*(iiter10+index1*numcat+index2-1))=0;

    //get related weights from variable in cplex model
    
	 cplex.getValues(x,vals);
     Delcon(index2,model,con,numcat,x);
	 model.add(con);

    //write robust assignments to xml file 
cout<<"write robust assignments to xml file..."<<endl; 
    if (*(iiter10+index1*numcat+index2-1)==1)	
		
		//if the assignment is possible, write it to the file		
		 {
         xmlNodePtr ggnode;
		 //create element
		 grandchild = xmlNewNode(NULL,BAD_CAST"element");
		 xmlAddChild(childnode,grandchild);

		 //create categoryID
		 ggnode = xmlNewNode(NULL,BAD_CAST"categoryID");
		 xmlAddChild(grandchild,ggnode);
		 xmlAddChild(ggnode,xmlNewText(BAD_CAST((*(iiter_catname+index2-1)).c_str())));

         //write relative weights and lamda to coment
		 //create values
		 ggnode = xmlNewNode(NULL,BAD_CAST"values");
		 xmlAddChild(grandchild,ggnode);

		   for (int s=0; s<=numcri; s++)
		   {
             xmlNodePtr g3node,g4node,g5node;
			//create values
		    g3node = xmlNewNode(NULL,BAD_CAST"value");
		    xmlAddChild(ggnode,g3node);

		   if(s==numcri)
		   xmlSetProp(g3node,BAD_CAST"name",BAD_CAST"lamda");
		   else 
		   xmlSetProp(g3node,BAD_CAST"name",BAD_CAST((*(iiter_catname+index2-1)).c_str()));

		   //g4node = xmlNewNode(NULL,BAD_CAST"value");
		   //xmlAddChild(g3node,g4node);

		   g4node = xmlNewNode(NULL,BAD_CAST"real");
		   xmlAddChild(g3node,g4node);
		   char *buf= new char[50];
		   sprintf(buf,"%f",vals[s]);
	       xmlAddChild(g4node,xmlNewText(BAD_CAST(buf)));		  


		   }// end of write relative weights and lamda to coment




}   //end of write robust assignments to xml file 

cout<<"Computation of robust assignment "<<altname[index1]<<" to "<<catname[index2-1]<<" is done!"<<endl;
 
   }//for index2


}//for index1

     
     outputName = argv[2];
	 szDocName = outputName;
	 szDocName+= "/alternativesAffectations_robustAssignments.xml";
	 writefile=xmlSaveFileEnc(szDocName.c_str(),docnew_alternativesAffectations2,"UTF-8");
	 if(writefile==-1)
	 {   cout<<"Failed to write result of alternativesAffectations of roustAssignments to xml file!"<<endl;
		 return NULL;}
	 else cout<<"The output file of alternativesAffectations of roustAssignments is saved..."<<endl; 
	 closeXmlFile(docnew_alternativesAffectations2);

     closeXmlFile(doc_alternatives);	
	 closeXmlFile(doc_alternativesAffectations);
	 closeXmlFile(doc_performanceTable);
	 closeXmlFile(doc_criteria);
	 closeXmlFile(doc_categoriesProfiles);
	 closeXmlFile(doc_criteriaLinearConstraints);
	 closeXmlFile(doc_categoriesComparisons);
	 closeXmlFile(doc_categories);
	 env.end();	
	 cout<<"The program is ended successfully!"<<endl; 
    
}// for try


  catch (IloException& e) {
	  
	  cerr << "Concert exception caught: " << e << endl;
	 
   }
   catch (...) {
	   cerr << "Exception caught: the information you provide is inconsistent, please check!" << endl;
   }

	 return 0;
}
