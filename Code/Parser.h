
#include <iconv.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
# include <libxml/xpathInternals.h>
#include <iostream>
# include <vector>


using namespace std;

#include <string>
using std::string;
  
   
xmlNodeSetPtr get_nodeset(xmlDocPtr doc, const char *xpath);


xmlDocPtr openXmlFile(const char* szXmlFilename);


void closeXmlFile(xmlDocPtr doc);


string getXmlString(xmlDocPtr doc, const char *szXpath);

int getXmlInt(xmlDocPtr doc, const char* szXpath);

double getXmlDouble(xmlDocPtr doc, const char* szXpath);


xmlNodeSetPtr get_nodeset(xmlDocPtr doc, const char *xpath) 
{
	xmlXPathContextPtr context;	
	xmlXPathObjectPtr result;
	xmlNodeSetPtr nodeSetPtr;
	
	context = xmlXPathNewContext(doc);
	if (context == NULL) 
	{	
		printf("context is NULL\n");
		return NULL; 
	}
	
   if(xmlXPathRegisterNs (context,  (xmlChar*)"xmcda", (xmlChar*)"http://www.decision-deck.org/2009/XMCDA-2.1.0") != 0) 
    {
       printf( "Unable to register NS " );
    }

	result = xmlXPathEvalExpression( (xmlChar *)xpath, context);
	xmlXPathFreeContext(context);
	if (result == NULL) 
	{
		cout<<xpath<<" does not exist, please check!"<<endl;
		return NULL;  
	}
	
	if (xmlXPathNodeSetIsEmpty(result->nodesetval)) 
	{
		xmlXPathFreeObject(result);
		cout<<"For"<<xpath;
		cout<<",Node set is empty, please check!"<<endl;
		return NULL;
	}
	

	nodeSetPtr = result->nodesetval;

	return nodeSetPtr;	
}

string getXmlString(xmlDocPtr doc, const char *szXpath)
{
	
	string strRel;
	xmlNodePtr nodePtr;
    xmlNodeSetPtr nodeSetPtr = get_nodeset(doc, szXpath); 
	
	if(nodeSetPtr==NULL)
	return "NULL";

    int i=nodeSetPtr->nodeNr;
	if(i==1)	
	nodePtr = nodeSetPtr->nodeTab[0];  
	else 
	{  
		nodePtr = nodeSetPtr->nodeTab[0];  
	}  

	
	strRel = (const char*)xmlNodeGetContent(nodePtr);
	
	return strRel;
}

int getXmlInt(xmlDocPtr doc, const char* szXpath)
{
	
	int iRel;
	xmlNodePtr nodePtr;
	xmlNodeSetPtr nodeSetPtr = get_nodeset(doc, szXpath); 

	
	if(nodeSetPtr==NULL)
	return 1e6;

	int i=nodeSetPtr->nodeNr;
	if(i==1)	
	nodePtr = nodeSetPtr->nodeTab[0];  
	else 
	{  
		nodePtr = nodeSetPtr->nodeTab[0];  
	}  
	
	iRel = atoi((const char*)xmlNodeGetContent(nodePtr));
	
	return iRel;
}

double getXmlDouble(xmlDocPtr doc, const char* szXpath)
{
	
	double dRel;
	xmlNodePtr nodePtr;
	xmlNodeSetPtr nodeSetPtr = get_nodeset(doc, szXpath); 

	
	if(nodeSetPtr==NULL)
	return 1e6;

	int i=nodeSetPtr->nodeNr;
	if(i==1)	
	nodePtr = nodeSetPtr->nodeTab[0];  
	else 
	{  
	    nodePtr = nodeSetPtr->nodeTab[0];  
	
	}

	dRel = atof ((const char*)xmlNodeGetContent(nodePtr));
	
	return dRel;
}

xmlDocPtr openXmlFile(const char* szXmlFilename)
{
	xmlDocPtr doc;   //
	xmlNodePtr curNodePtr;  //
	
	doc = xmlReadFile(szXmlFilename,NULL,XML_PARSE_RECOVER);  // 
	if (doc == NULL ) 
	{
		fprintf(stderr,"Document not parsed successfully. \n"); 	
		xmlFreeDoc(doc); 
		exit(1);
	} 	
	curNodePtr = xmlDocGetRootElement(doc);  //
	

	if (curNodePtr == NULL)
	{
		fprintf(stderr,"empty document\n"); 
		xmlFreeDoc(doc); 
		exit(1);
	}
	
		
	if (xmlStrcmp(curNodePtr->name, (const xmlChar *) "XMCDA")) 
	{ 
		fprintf(stderr,"document of the wrong type, root node != XMCDA"); 
		xmlFreeDoc(doc); 
		exit(1);
	} 

	return doc;
}

void closeXmlFile(xmlDocPtr doc)
{
	xmlFreeDoc(doc); 
}


xmlDocPtr createXmlFile()
{
	

 xmlDocPtr doc = xmlNewDoc(BAD_CAST"1.0");
 //xmlNodePtr stylesheet = xmlNewDocPI(doc,BAD_CAST"xml-stylesheet",BAD_CAST"type=\"text/xsl\" href=\"XMCDA.xsl\"");
 xmlNodePtr root_node = xmlNewNode(NULL,BAD_CAST"xmcda:XMCDA");
 xmlSetProp(root_node,BAD_CAST"xmlns:xsi",BAD_CAST"http://www.w3.org/2001/XMLSchema-instance");
 xmlSetProp(root_node,BAD_CAST"xsi:schemaLocation",BAD_CAST"http://www.decision-deck.org/2009/XMCDA-2.1.0 xmcda%20shema.xsd");
 xmlSetProp(root_node,BAD_CAST"xmlns:xmcda",BAD_CAST"http://www.decision-deck.org/2009/XMCDA-2.1.0");
 xmlDocSetRootElement(doc,root_node); 
 //xmlAddPrevSibling(root_node,stylesheet);//insert a processing instruction(pi) element to xml file


return doc;


}

